﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lifter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating ("randompos", 3f, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.up * Time.deltaTime * 3f;
     
    }

    void randompos ()
    {
        transform.position = new Vector3(Random.Range(-5, 5), -4, Random.Range(-7, 7));
    }
}

